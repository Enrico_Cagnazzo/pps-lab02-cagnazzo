package u02lab.code;

import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

/**
 * Created by Enrico on 07/03/2017
 */
public class RandomGeneratorTest {
    final static int LENGTH=11;
    private static RandomGenerator randomGenerator;
    private ConcreteFactory factory=new ConcreteFactory();

    @Before
    public void init(){
        randomGenerator=factory.createRandomGenerator(LENGTH);
    }

    @Test
    public void checkAllTheStreamOfBit(){
        Optional<Integer> nextBit;
        for (int i = 0; i< LENGTH; i++){
            nextBit=randomGenerator.next();
            assertTrue(nextBit.isPresent());
            assertTrue(nextBit.get()==0||nextBit.get()==1);
        }
        nextBit=randomGenerator.next();
        assertFalse(nextBit.isPresent());
    }

    private void checkCorrectnessOfRemainingList(){
        List<Integer> expectedList=randomGenerator.allRemaining();
        for (int item:expectedList) {
            Optional<Integer> next=randomGenerator.next();
            assertTrue(next.isPresent());
            assertTrue(item==next.get());
        }
    }

    @Test
    public void checkCorrectnessOfRemainingListAtTheBegin() throws Exception{
        checkCorrectnessOfRemainingList();
    }

    @Test
    public void checkCorrectnessOfRemainingListAfterHalfSlide() throws Exception{
        UtilityForTest.halfSlide(randomGenerator);
        checkCorrectnessOfRemainingList();
    }

}