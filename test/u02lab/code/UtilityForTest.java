package u02lab.code;

/**
 * Created by Enrico on 07/03/2017
 */

class UtilityForTest {

    static void halfSlide(SequenceGenerator generator){
        UtilityForTest.slideOfXPositions(generator, generator.getHalfLength());
    }

    static void fullSlide(SequenceGenerator generator){
        UtilityForTest.slideOfXPositions(generator, generator.getFullLength());
    }

    private static void slideOfXPositions(SequenceGenerator generator, int x){
        for (int i=0;i<x;i++){
            generator.next();
        }
    }

}
