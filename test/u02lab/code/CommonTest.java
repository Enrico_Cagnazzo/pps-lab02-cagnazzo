package u02lab.code;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static u02lab.code.RangeGeneratorTest.*;
import static u02lab.code.RandomGeneratorTest.*;

/**
 * Created by Enrico on 07/03/2017
 */
public class CommonTest {
    private static RangeGenerator rangeGenerator;
    private static RandomGenerator randomGenerator;
    private ConcreteFactory factory=new ConcreteFactory();


    @Before
    public void init(){
        rangeGenerator=factory.createRangeGenerator(START,STOP);
        randomGenerator=factory.createRandomGenerator(LENGTH);
    }

    private void checkIsOverAfterHalfSlide(SequenceGenerator sequenceGenerator){
        UtilityForTest.halfSlide(sequenceGenerator);
        assertFalse(sequenceGenerator.isOver());
    }

    @Test
    public void checkIsOverAfterHalfSlide(){
        checkIsOverAfterHalfSlide(rangeGenerator);
        checkIsOverAfterHalfSlide(randomGenerator);
    }

    private void checkIsOverAfterFullSlide(SequenceGenerator sequenceGenerator){
        UtilityForTest.fullSlide(sequenceGenerator);
        assertTrue(sequenceGenerator.isOver());
    }

    @Test
    public void checkIsOverAfterFullSlide(){
        checkIsOverAfterFullSlide(rangeGenerator);
        checkIsOverAfterFullSlide(randomGenerator);
    }

    private void checkResetAfterFullSlide(SequenceGenerator generator){
        UtilityForTest.fullSlide(generator);
        generator.reset();
        assertFalse(generator.isOver());
    }

    @Test
    public void checkResetAfterFullSlide(){
        checkResetAfterFullSlide(rangeGenerator);
        checkResetAfterFullSlide(randomGenerator);
    }


    private void checkRemainingListLength(SequenceGenerator generator){
        UtilityForTest.halfSlide(generator);
        assertFalse(generator.allRemaining().isEmpty());
        generator.reset();
        UtilityForTest.fullSlide(generator);
        assertTrue(generator.allRemaining().isEmpty());
    }

    @Test
    public void checkRemainingListLength(){
        checkRemainingListLength(rangeGenerator);
        checkRemainingListLength(randomGenerator);
    }

    private void checkRemainingAfterCompleteSlide(SequenceGenerator generator){
        UtilityForTest.fullSlide(generator);
        assertTrue(generator.allRemaining().equals(new ArrayList<Integer>()));
    }

    @Test
    public void checkRemainingAfterCompleteSlide(){
        checkRemainingAfterCompleteSlide(rangeGenerator);
        checkRemainingAfterCompleteSlide(randomGenerator);
    }
}
