package u02lab.code;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

/**
 * Created by Enrico on 07/03/2017
 */

public class RangeGeneratorTest {
    final static int START=1;
    final static int STOP=10;
    private static RangeGenerator rangeGenerator;
    private ConcreteFactory factory=new ConcreteFactory();

    private static int getHalfSize(){
        return getFullSize()/2;
    }

    private static int getFullSize(){
        return STOP-START+1;
    }

    @Before
    public void init(){
        rangeGenerator=factory.createRangeGenerator(START,STOP);
    }

    @Test
    public void isNotOverAfterCorrectCreation(){
        assertFalse(rangeGenerator.isOver());
    }

    @Test
    public void isOverAfterWrongCreation(){
        rangeGenerator=new RangeGenerator(STOP,START);
        assertTrue(rangeGenerator.isOver());
    }

    @Test
    public void isListCorrect(){
        Optional<Integer> nextNumber;
        for (int i=START;i<=STOP;i++){
            nextNumber=rangeGenerator.next();
            assertTrue(nextNumber.isPresent());
            assertTrue(nextNumber.get()==i);
        }
        assertFalse(rangeGenerator.next().isPresent());
    }

    @Test
    public void useResetAfterHalfSlide(){
        UtilityForTest.halfSlide(rangeGenerator);
        rangeGenerator.reset();
        assertTrue(rangeGenerator.next().get()==START);
    }

    @Test
    public void checkRemainingListAfterHalfSlide(){
        UtilityForTest.halfSlide(rangeGenerator);
        List<Integer> remainingList=new ArrayList<>();
        for (int i=START+getHalfSize();i<=STOP;i++) {
            remainingList.add(i);
        }
        assertTrue(remainingList.equals(rangeGenerator.allRemaining()));
    }

}