package u02lab.code;

/**
 * Created by Enrico on 07/03/2017
 */
public interface AbstractFactory {
    RangeGenerator createRangeGenerator(int start,int stop);
    RandomGenerator createRandomGenerator(int length);
}
