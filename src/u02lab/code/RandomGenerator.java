package u02lab.code;

import java.util.*;

/**
 * Step 2
 *
 * Forget about class u02lab.code.RangeGenerator. Using TDD approach (create small test, create code that pass test, refactor
 * to excellence) implement the below class that represents a sequence of n random bits (0 or 1). Recall
 * that math.random() gives a double in [0,1]..
 * Be sure to test all that is needed, as before
 *
 * When you are done:
 * A) try to refactor the code according to DRY (u02lab.code.RandomGenerator vs u02lab.code.RangeGenerator)?
 * - be sure tests still pass
 * - refactor the test code as well
 * B) create an abstract factory for these two classes, and implement it
 */
public class RandomGenerator implements SequenceGenerator {

    private final int length;
    private LinkedList<Integer> remainingList=null;

    RandomGenerator(int n){
        length=n;
    }

    private LinkedList<Integer> createRandomList() {
        LinkedList<Integer> newList=new LinkedList<>();
        for (int i=0;i<length;i++){
            newList.add(Math.round(((float) Math.random())));
        }
        return newList;
    }

    @Override
    public Optional<Integer> next() {
        if (remainingList==null){
            remainingList=createRandomList();
        }
        if (remainingList.isEmpty())
            return Optional.empty();
        return Optional.of(remainingList.removeFirst());
    }

    @Override
    public void reset() {
        remainingList=null;
    }

    @Override
    public boolean isOver() {
        return remainingList!=null && remainingList.isEmpty();
    }

    @Override
    public List<Integer> allRemaining() {
        if (remainingList==null)
            remainingList=createRandomList();
        return Collections.unmodifiableList(new ArrayList<>(remainingList));
    }

    @Override
    public int getHalfLength() {
        return length/2;
    }

    @Override
    public int getFullLength() {
        return length;
    }
}
