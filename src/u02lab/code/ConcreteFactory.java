package u02lab.code;

/**
 * Created by Enrico on 07/03/2017
 */

public class ConcreteFactory implements AbstractFactory {

    @Override
    public RangeGenerator createRangeGenerator(int start,int stop) {
        return new RangeGenerator(start,stop);
    }

    @Override
    public RandomGenerator createRandomGenerator(int length) {
        return new RandomGenerator(length);
    }
}
