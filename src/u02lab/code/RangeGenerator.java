package u02lab.code;

import java.util.*;

/**
 * Step 1
 *
 * Using TDD approach (create small test, create code that pass test, refactor to excellence)
 * implement the below class that represents the sequence of numbers from start to stop included.
 * Be sure to test that:
 * + the produced elements (called using next(), go from start to stop included)
 * + calling next after stop has been produced, lead to a Optional.empty
 * + calling reset after producing some elements brings the object back at the beginning
 * + isOver can actually be called in the middle and gives false, at the end and gives true
 * + can produce the list of remaining elements in one shot
 */
public class RangeGenerator implements SequenceGenerator {

    private int current;
    private int start;
    private int stop;

    RangeGenerator(int start, int stop){
        this.current=start;
        this.start=start;
        this.stop=stop;
    }

    @Override
    public Optional<Integer> next() {
        if (this.isOver())
            return Optional.empty();
        return Optional.of(current++);
    }

    @Override
    public void reset() {
        this.current=this.start;
    }

    @Override
    public boolean isOver() {
        return current>stop;
    }

    @Override
    public List<Integer> allRemaining() {
        List<Integer> remaining=new ArrayList<>();
        for (int i=current;i<=stop;i++)
            remaining.add(i);
        return Collections.unmodifiableList(remaining);
    }

    @Override
    public int getHalfLength() {
        return getFullLength()/2;
    }

    @Override
    public int getFullLength() {
        return stop-start+1;
    }

}
